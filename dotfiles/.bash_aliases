# Modified commands
alias grep='grep --color=auto'

# vim
alias vi='vim'

# ls
alias ll='ls -l'

# Make bash error tolerant
alias :q=' exit'
alias :Q=' exit'
