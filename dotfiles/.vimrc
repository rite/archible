set nocompatible

scriptencoding utf-8
set encoding=utf-8

" always show status line
set laststatus=2

"set so=8

" pathogen.vim - the poor man's package manager
execute pathogen#infect()

" line numbering and highlighting
set relativenumber number
set cul
set wrap

" indentation
set tabstop=4
set autoindent smartindent
set shiftwidth=4
"set expandtab

filetype plugin indent on

" search behavior
set incsearch ignorecase smartcase hlsearch


" beautiful colors
"let base16colorspace=256
"colorscheme base16-default-dark
syntax on

" syntax highlighting for special files
au BufReadPost wscript set syntax=python

" show unexpected whitespaces
set list listchars=tab:»\ ,trail:·

" highlight the 81st character of a line
call matchadd('warningmsg', '\%81v', 100)

" key mappings
let mapleader = "\<Space>"
nnoremap <C-p> :Unite -start-insert file_rec/async<CR>
nnoremap <C-f> :Unite -start-insert file_rec/git<CR>
nnoremap <Leader>/ :Unite grep:.<CR>
nnoremap <Leader>b :Unite buffer<CR>

noremap <Leader>e :Unite -start-insert buffer file file_rec<CR>
noremap <Leader>t :NERDTree<CR>
inoremap <S-Tab> <C-V><Tab>

" tab-completion of ex commands (bash-like)
set wildmode=longest,list

" mouse support
set ttyfast
set mouse=a
set ttymouse=xterm2

" airline - lean & mean status/tabline for vim that's light as air
"let g:airline_theme='base16'
let g:airline_powerline_fonts = 1
let g:airline#extensions#bufferline#enabled = 0

"  only show the tail, e.g. a branch 'feature/foo' show 'foo'
let g:airline#extensions#branch#format = 1
